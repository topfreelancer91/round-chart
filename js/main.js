  $(document).ready(function () {
    var currentValue = 42;

    $("#type").roundSlider({
      radius: 100,
      width: 5,
      handleSize: "24,24",
      handleShape: "#14B92A",
      sliderType: "min-range",
      startAngle: 90,
      value: currentValue,
      editableTooltip: false
    });

    $(".rs-handle").html('<span class="rs-handle-lines"></span><span class="rs-handle-lines"></span><span class="rs-handle-lines"></span>');
    $("#type").append('<span class="tooltip-text">Minimum Attendance Required</span>');

    $("a[name=edit]").on('click', function(){
      $('.rs-handle').show();
      $('a[name=done]').show();
      $('a[name=cancel]').show();
      $('a[name=edit]').hide();
      currentValue = $("#type").roundSlider('getValue');
      $("#type").roundSlider('option', 'block', true);
    });

    $("a[name=cancel]").on('click', function(){
      $('.rs-handle').hide();
      $('a[name=done]').hide();
      $('a[name=cancel]').hide();
      $('a[name=edit]').show();
      $("#type").roundSlider('setValue', currentValue);
      $("#type").roundSlider('option', 'block', false);
    });

    $("a[name=done]").on('click', function(){
      $('.rs-handle').hide();
      $('a[name=done]').hide();
      $('a[name=cancel]').hide();
      $('a[name=edit]').show();
      currentValue = $("#type").roundSlider('getValue');
      $("#type").roundSlider('option', 'block', false);
    });

  });
